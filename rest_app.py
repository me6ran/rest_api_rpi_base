from flask import Flask,jsonify,abort,make_response,request,url_for,g
from flask_sqlalchemy import SQLAlchemy
from passlib.hash import pbkdf2_sha256
from flask_httpauth import HTTPBasicAuth
import os
from rpi_base import RPI_BASE

my_base = RPI_BASE()

app = Flask(__name__)
db__path = os.path.join(os.path.dirname(__file__),"app.db")
db_uri = "sqlite:///{}".format(db__path)

app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)
auth = HTTPBasicAuth()

############ defining database and table
class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), index=True)
    password_hash = db.Column(db.String(64))

    def hash_password(self, password):
        self.password_hash = pbkdf2_sha256.encrypt(password)

    def verify_password(self, password):
        return pbkdf2_sha256.verify(password, self.password_hash)

########### verification block
@auth.verify_password
def verify_password(username, password):
    user = User.query.filter_by(username=username).first()
    if not user or not user.verify_password(password):
        return False
    g.user = user
    return True

############### apis 
@app.route("/api/users", methods=['POST'])
def new_user():
    username = request.json.get('username')
    password = request.json.get('password')
    if username is None or password is None:
        abort(400)
    if User.query.filter_by(username=username).first() is not None:
        abort(400)
    user = User(username=username)
    user.hash_password(password)
    db.session.add(user)
    db.session.commit()
    return (jsonify({'username':user.username}),201,
    {'Location': url_for('get_user', id=user.id, _external=True)})

@app.route("/api/users/<int:id>")
def get_user(id):
    user = User.query.get(id)
    if not user:
        abort(400)
    return jsonify({'username': user.username})

@app.route("/api/relay=<int:id>/energize",methods=["GET"])
@auth.login_required
def energize_relay(id):
    if(my_base.energize_relay(id)):
        return make_response("Done",200)
    else:
        return abort(404)

@app.route("/api/relay=<int:id>/denergize",methods=["GET"])
@auth.login_required
def denergize_relay(id):
    if(my_base.denergize_relay(id)):
        return make_response("Done",200)
    else:
        return abort(404)

@app.route("/",methods=["GET"])
def show_home():
    return make_response("",200)


    
@app.route("/api/di=<string:id>/state",methods=["GET"])
def check_DI(id):
    state = my_base.check_DI(id)
    if state==False:
        return abort(404)
    return jsonify({"Digital Input": state})

@auth.login_required
def active_do(do_number):
    if(my_base.active_DO(do_number)):
        return jsonify({"Digital output":"output: %s is active" % (do_number)})
    else:
        abort(404)

@auth.login_required
def deactive_do(do_number):
    if(my_base.deactive_DO(do_number)):
        return jsonify({"Digital output":"output: %s is deactivated" % (do_number)})
    else:
        abort(404)

app.add_url_rule("/api/do=<int:do_number>/active","active_do",active_do, methods=["GET"])
app.add_url_rule("/api/do=<int:do_number>/deactive","deactive_do",deactive_do, methods=["GET"])

if not os.path.exists('db.sqlite'):
        db.create_all()