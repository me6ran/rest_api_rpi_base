import RPi.GPIO as IO


class RPI_BASE:
    ADC_CHANN_NUM = 4
    NUM_OF_READ = 5
    NUM_OF_REL = 4
    NUM_OF_INPUT = 5
    NUM_OF_OUTPUT = 5
    out_pin_dict =  {1:17,2:27,3:12,4:19}
    dout_pin_dict = {1:21,2:20,3:25,4:26,5:24}
    din_pin_dict = {1:23,2:18,3:5,4:4,5:6}
    def __init__(self):
        IO.setmode(IO.BCM)
        IO.setwarnings(False)
        IO.setup(list(RPI_BASE.out_pin_dict.values()),IO.OUT,initial=IO.LOW)
        ret = [IO.setup(pin,IO.IN) for pin in RPI_BASE.din_pin_dict.values()]    
        ret = [IO.setup(pin,IO.OUT) for pin in RPI_BASE.dout_pin_dict.values()]
        del ret

    def energize_relay(self,relay_number):
        if relay_number>RPI_BASE.NUM_OF_REL:
            return False
        IO.output(RPI_BASE.out_pin_dict[relay_number],IO.HIGH)
        return True

    def energize_all_relay(self):
        print("energizing all")
        ret = list(map(self.energize_relay,RPI_BASE.out_pin_dict.keys()))
        if False in ret:
            return False
        else:
            return True

    def denergize_relay(self,relay_number):
        if relay_number>RPI_BASE.NUM_OF_REL:
            return False
        IO.output(RPI_BASE.out_pin_dict[relay_number],IO.LOW)
        return True
    
    def denergize_all_relay(self):
        ret= list(map(self.denergize_relay,RPI_BASE.out_pin_dict.keys()))
        if False in ret:
            return False
        else:
            return True


    def check_DI(self,input_string):
        try:
            input_number=list(map(int,input_string.split(",")))
        except:
            return False
        di_dict={}
        for pin in input_number:
            if pin>RPI_BASE.NUM_OF_INPUT:
                return False
            state = ['Low','High'][IO.input(RPI_BASE.din_pin_dict[pin])]
            di_dict.update({pin:state})
        return di_dict
    
    
    def active_DO(self,do_number):
        if(do_number>RPI_BASE.NUM_OF_OUTPUT):
            return False
        IO.output(RPI_BASE.dout_pin_dict[do_number],IO.HIGH)
        return True

    def deactive_DO(self,do_number):
        if(do_number>RPI_BASE.NUM_OF_OUTPUT):
            return False
        IO.output(RPI_BASE.dout_pin_dict[do_number],IO.LOW)
        
        return True